Getting Started:

1: Clone the Repository and open up a bash terminal in the root project directory

2: Execute "run.sh" to export the required environment variable and start up the flask app, then follow the localhost link to view it.
