from setuptools import setup

setup(
    name='CalibrationPro',
    packages=['CalibrationPro'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
