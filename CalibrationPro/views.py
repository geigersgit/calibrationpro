from CalibrationPro import app
from flask import render_template, request, redirect


@app.route("/")
@app.route("/index")
def index():
    return render_template('index.html')


#divide voltage and current by 100 / mod decimal places
#data/100 . data%100
#re = reset to defaults
#vscode
