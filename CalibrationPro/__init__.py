from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from time import sleep
import socket

from random import random
from time import sleep
import threading# import Thread, Event

import os

os.putenv('FLASK_APP', 'CalibrationPro')

app = Flask(__name__)
socketio = SocketIO(app)

thread = threading.Thread()

MSG = "so1"
UDP_IP = "126.10.218.10"
UDP_PORT = 2002

#UDP_IP = "10.0.0.245"
#UDP_PORT = 54253

arduino = None

class RandomThread(threading.Thread):
    def __init__(self):
        self.delay = 2
        threading.Thread.__init__(self)
        self.thread_stop_event = threading.Event()

    def randomNumberGenerator(self):
        #infinite loop of magical random numbers
        while not self.thread_stop_event.isSet():
            global arduino
            rVoltage = 0
            rCurrent = 0
            
            Voltage = 0
            Current = 0

            #Get Voltage
            MSG = "ru"
            arduino.sendto(MSG, (UDP_IP, UDP_PORT))
            rVoltage, addr = arduino.recvfrom(1024)
            sleep(self.delay)    
            arduino.sendto(MSG, (UDP_IP, UDP_PORT))
            rVoltage, addr = arduino.recvfrom(1024)

            #Get Current
            MSG = "ri"
            arduino.sendto(MSG, (UDP_IP, UDP_PORT))
            rCurrent, addr = arduino.recvfrom(1024)
            sleep(self.delay)
            arduino.sendto(MSG, (UDP_IP, UDP_PORT))
            rCurrent, addr = arduino.recvfrom(1024)
            
            print "RECIEVED VOLTAGE: "
            Voltage  = float(rVoltage)
            Voltage /= 100
            #print Voltage
            print "RECIEVED CURRENT: "
            #print Current
            Current  = float(rCurrent)
            Current /= 100
            
            socketio.emit('newData', {'voltage': Voltage, 'current': Current})
            print "SENT DATA TO CLIENT"
            sleep(self.delay)

    def run(self):
        self.randomNumberGenerator()

    def stop(self):
        self.thread_stop_event.set()
        self.join()

    def stopped(self):
        return self.thread_stop_event.is_set()

def formatFloat(string):
    string = "{:4.2f}".format(float(string))
    if(float(string) < 10):
        string = "0" + string
    print string
    string = string[0] + string[1] + string[3] + string[4]
    return string

@socketio.on('connect')
def test_connect():
    print('Client connected')


@socketio.on('connectToPS')
def connectToPS(data):
    print(data['baudRate'])
    print(data['ipAddress'])
    print(data['letterAddress'])
    print(data['productNumber'])
    print(data['model'])

    #UDP_IP = data['ipAddress']

    global arduino
    arduino = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    MSG = "co{0}".format((ord(data['letterAddress']) - 65))
    arduino.sendto(MSG, (UDP_IP, UDP_PORT))

    successful = True
    #if successful:

@socketio.on('on')
def turnOn(data):

    print(data['voltage'])
    print(data['current'])
    voltageString = formatFloat(data['voltage'])
    currentString = formatFloat(data['current'])

    print("SENDING ON COMMAND")
    MSG = "so1"
    arduino.sendto(MSG, (UDP_IP, UDP_PORT))
    sleep(1)

    print("SENDING VOLTAGE SET COMMAND")
    MSG = "su" + voltageString
    arduino.sendto(MSG, (UDP_IP, UDP_PORT))
    sleep(1)

    print("SENDING CURRENT SET COMMAND")
    MSG = "si" + currentString
    arduino.sendto(MSG, (UDP_IP, UDP_PORT))
    sleep(1)

    global thread
    if not thread.isAlive():
        print "Starting Thread"
        thread = RandomThread()
        thread.start()

@socketio.on('off')
def turnOff():
    print("SENDING OFF COMMAND")
    MSG = "so0"
    global arduino
    arduino.sendto(MSG, (UDP_IP, UDP_PORT))
    global thread
    thread.stop()

    # MSG = "ru"
    # global arduino
    # arduino.sendto(MSG, (UDP_IP, UDP_PORT))
    # data = 0
    # while (data == 0):
    #     data, addr = arduino.recvfrom(1024)
    # print "received message:", data

@socketio.on('restart')
def restart():
    print("SENDING RESTART COMMAND")


import CalibrationPro.views
