"use strict";
google.charts.load('current', {packages: ['corechart', 'line']});

$(document).ready(function(){

  var socket = io.connect('http://' + document.domain + ':' + location.port);
  var voltage_received = [];
  var current_received = [];
  var time = 0;

  var voltage_table;
  var current_table;
  var voltage_options;
  var current_options;
  var voltage_chart;
  var current_chart;

  function drawChart(){

    voltage_table = new google.visualization.DataTable();
    current_table = new google.visualization.DataTable();

    voltage_table.addColumn('number', 'time');
    voltage_table.addColumn('number', 'voltage');

    current_table.addColumn('number', 'time');
    current_table.addColumn('number', 'current');

    voltage_options = {
      hAxis: {
        title: 'Time',
        viewWindow: {
        min: 0,
        max: 1000
      }},
      vAxis: {
        title: 'Voltage',
        viewWindow: {
          min: 0,
          max: 100
        }
      },
      animation:{duration: 1000, easing: 'out'}
    };
    current_options = {
      hAxis: {
        title: 'Time',
        viewWindow: {
        min: 0,
        max: 1000
      }},
      vAxis: {
        title: 'Current',
        viewWindow: {
          min: 0,
          max: 100
        }
      },
      animation:{duration: 1000, easing: 'out'},
      colors: ['red']
    };

    voltage_chart = new google.visualization.LineChart(document.getElementById('voltage_chart_div'));
    current_chart = new google.visualization.LineChart(document.getElementById('current_chart_div'));
    voltage_chart.draw(voltage_table, voltage_options);
    current_chart.draw(current_table, current_options);

    //triggers when the "newData" event is emitted by the server.
    socket.on('newData', function(msg) {
        console.log("new data");
        console.log(msg.voltage);
        console.log(msg.current);
        voltage_received.push([time, msg.voltage]);
        current_received.push([time, msg.current]);

        document.getElementById("mVoltage").value = msg.voltage;
        document.getElementById("mCurrent").value = msg.current;
        if (voltage_received.length >= 100){
              voltage_received.shift();
              voltage_table = new google.visualization.DataTable();
              voltage_table.addColumn('number', 'time');
              voltage_table.addColumn('number', 'voltage');
              voltage_table.addRows(voltage_received);
              var alt_voltage_options = {
                hAxis: {
                  title: 'Time',
                  viewWindow: {
                  min: (time-1010),
                  max: time
                }},
                vAxis: {
                  title: 'Voltage',
                  viewWindow: {
                    min: 0,
                    max: 100
                  }
                },
                animation:{duration: 1000, easing: 'out'}
              };
              voltage_chart.draw(voltage_table, alt_voltage_options);
        }
        else{
          voltage_table.addRow([time, msg.voltage]);
          voltage_chart.draw(voltage_table, voltage_options);
        }
        if (current_received.length >= 100){
              current_received.shift();
              current_table = new google.visualization.DataTable();
              current_table.addColumn('number', 'time');
              current_table.addColumn('number', 'current');
              current_table.addRows(current_received);
              var alt_current_options = {
                hAxis: {
                  title: 'Time',
                  viewWindow: {
                  min: (time-1010),
                  max: time
                }},
                vAxis: {
                  title: 'current',
                  viewWindow: {
                    min: 0,
                    max: 100
                  }
                },
                animation:{duration: 1000, easing: 'out'}
              };
              current_chart.draw(current_table, alt_current_options);
        }
        else{
          current_table.addRow([time, msg.current]);
          current_chart.draw(current_table, current_options);
        }

        time += 10;
    })
  }

google.charts.setOnLoadCallback(drawChart);

////////////////////////////////////////////////////////////////////////////////

  var homePage = (function(){

    var tabBar, configMenu, i;
    var onButton, offButton, restartButton, connectButton;

    var getDomElements = function(){
      tabBar = document.getElementById("tabBar");
      onButton = document.getElementById("onButton");
      offButton = document.getElementById("offButton");
      restartButton = document.getElementById("restartButton");
      connectButton = document.getElementById("connectButton");
      configMenu = document.getElementById("Configuration");
    };

    var assignActionListeners = function(){

      tabBar.addEventListener("click", function(event){
        if(this != event.target){
          var tabs = tabBar.getElementsByClassName("tablinks");
          for (i = 0; i < tabs.length; i++) {
            tabs[i].className = tabs[i].className.replace(" active", "");
          }
        }
        event.target.className += " active";
        if(event.target.id === "ConfigurationTab"){
          document.getElementById("Configuration").style.display = "block";
        }
        else if(event.target.id === "ControlTab"){
          document.getElementById("Configuration").style.display = "none";
        }
      }, true);

      onButton.addEventListener("click", function(){
        var tVoltage = document.getElementById("tVoltage").value;
        var tCurrent = document.getElementById("tCurrent").value;
        socket.emit('on', {
          'voltage':tVoltage,
          'current':tCurrent
        });
      });

      offButton.addEventListener("click", function(){
        socket.emit('off');
      });

      restartButton.addEventListener("click", function(){
        socket.emit('restart');
        time = 0;
        voltage_table = new google.visualization.DataTable();
          voltage_table.addColumn('number', 'time');
          voltage_table.addColumn('number', 'voltage');
          voltage_received = [];
          voltage_chart.draw(voltage_table, voltage_options);
        current_table = new google.visualization.DataTable();
          current_table.addColumn('number', 'time');
          current_table.addColumn('number', 'current');
          current_received = [];
          current_chart.draw(current_table, current_options);
      });

      connectButton.addEventListener("click", function(){

          var baudRate = document.getElementById("baudRate").value;
          var ipAddress = document.getElementById("ipAddress").value;
          var letterAddress = document.getElementById("letterAddress").value;
          var productNumber = document.getElementById("productNumber").value;
          var model = document.getElementById("model").value;

        socket.emit('connectToPS', {
          'baudRate':baudRate,
          'ipAddress':ipAddress,
          'letterAddress':letterAddress,
          'productNumber':productNumber,
          'model':model
        });
      });
   };

   var init = function (){
     getDomElements();
     assignActionListeners();
   };

   return {
     init: init
   };
  })();
homePage.init();

////////////////////////////////////////////////////////////////////////////////
});
